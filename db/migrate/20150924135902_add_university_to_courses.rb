class AddUniversityToCourses < ActiveRecord::Migration
  def change
  	rename_table :universities, :institutions
  	uni = Institution.create! name: "University of Nairobi"
  	rename_table :institutions, :universities
    add_reference :courses, :university, index: true, foreign_key: true, default: uni.id
  end
end
