class AddAttachmentHtmlToSubtopics < ActiveRecord::Migration

	def self.up
    create_table :subtopics, id: :uuid do |t|
      t.text :title
      # t.deleted_at :datetime
      t.attachment :html

      t.timestamps null: false
    end
  end

  # add_column :subtopics, :deleted_at, :datetime
  # add_index :subtopics, :deleted_at

  # def self.up
  #   change_table :subtopics do |t|
  #     t.attachment :html
  #   end
  # end

  # def self.down
  #   remove_attachment :subtopics, :html
  # end
end