class AddQuestionToResponses < ActiveRecord::Migration

   # create subtopics

	def change
    create_table :subtopics, id: :uuid do |t|
      t.text :title

      t.timestamps null: false
    end
    sub = Subtopic.create! title: " Subtopic 1", topic_id: "#{Topic.first.id}"
  end

   # AddUnitidToTopics
 
  def change
  	# topic = Topic.create! name: "Capital Budgeting", code: "AAAAA", unit: Unit.first
  	remove_index :topics, :unit_id
  	remove_column :topics, :unit_id, :integer
  	add_column :topics, :unit_id, :uuid
  	# topic = Topic.create! name: "Capital Budgeting", code: "AAAAA", unit: Unit.first
  	add_index :topics, :unit_id #, :default => topic.id
  end

  #AddTopicsToSubtopics

  def change
  	add_column :subtopics, :unit_id, :uuid
  	add_index :subtopics, :unit_id 
    
  end

  #ChangeUnitIdToTopicId 
  def change
  	rename_column :subtopics, :unit_id, :topic_id
  end

  #AddSubtopicToQuestion 
  def change
  	add_column :questions, :subtopic_id, :uuid
  end



  def change
  	
  	question = Question.create! question: " Why are you in school", exam_id: "#{Unit.first.id}"
    add_reference :responses, :question, index: true, foreign_key: true, default: question.id
  end
end
